/**
 * 
 */
package artemislitegame;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * @author Brian Penrose
 *
 */
public class Player implements IDisplay {

	String playerName;
	int playerResources;
	int currentPosition;
	int playerTurnOrderPosition;
	ArrayList<Integer> ownedSquares = new ArrayList<Integer>();

	/**
	 * Default constructor
	 */
	public Player() {
	}

	/**
	 * Constructor setting up player with name.
	 * 
	 * @param playerName
	 */
	public Player(String playerName) {
		this.playerName = playerName;
		playerResources = 200;
		currentPosition = 1;
	}

	/**
	 * @return the playerName
	 */
	public String getPlayerName() {
		return playerName;
	}

	/**
	 * @param playerName the playerName to set
	 */
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	/**
	 * @return the playerResources
	 */
	public int getPlayerResources() {
		return playerResources;
	}

	/**
	 * @param playerResources the playerResources to set
	 */
	public void setPlayerResources(int playerResources) {
		this.playerResources = playerResources;
	}

	/**
	 * @return the currentPosition
	 */
	public int getCurrentPosition() {
		return currentPosition;
	}

	/**
	 * @param currentPosition the currentPosition to set
	 */
	public void setCurrentPosition(int currentPosition) {
		this.currentPosition = currentPosition;
	}

	/**
	 * @return the playerTurnOrderPosition
	 */
	public int getPlayerTurnOrderPosition() {
		return playerTurnOrderPosition;
	}

	/**
	 * @param playerTurnOrderPosition the playerTurnOrderPosition to set
	 */
	public void setPlayerTurnOrderPosition(int playerTurnOrderPosition) {
		this.playerTurnOrderPosition = playerTurnOrderPosition;
	}

	/**
	 * Retrieves the list containing the IDs of owned squares
	 * 
	 * @return the ownedSquares
	 */
	public ArrayList<Integer> getOwnedSquares() {
		return ownedSquares;
	}

	/**
	 * @param ownedSquares the ownedSquares to set
	 */
	public void setOwnedSquares() {
	}

	/**
	 * Overridden toString that returns Player details as a human readable String.
	 */
	@Override
	public String display() {
		return "--------------------------------------------------------" + "\nName: " + this.playerName
				+ "\nR&D Capacity: " + this.playerResources + "\nCurrent Position: " + this.currentPosition
				+ "\nContracts: " + this.ownedSquares.toString()
				+ "\n--------------------------------------------------------\n";
	}

	/**
	 * The player rolls two simulated six-sided dice and adds the result together.
	 * They are moved this many squares, in sequence, from their current position.
	 * Their currentPosition attribute is set as their new position and they are
	 * removed from the sojourners list from the old square and added to the new
	 * one. If The rolled result would bring them to a square above ID 12 the roll
	 * is modified by -12 to bring them to the start of the board again, looping the
	 * board. If this occurs they will collect 100 resources.
	 * 
	 * @param game
	 * @param scan
	 */
	public void move(Game game, Scanner scan) {
		int moveRollOne, moveRollTwo, newPosition;
		System.out.println("Rolling for " + this.getPlayerName() + "'s turn...");
		moveRollOne = (int) (1 + Math.random() * 6);
		moveRollTwo = (int) (1 + Math.random() * 6);
		System.out.println("You rolled a " + moveRollOne + " and a " + moveRollTwo + " for a total of "
				+ (moveRollOne + moveRollTwo) + ".");

		if ((this.getCurrentPosition() + moveRollOne + moveRollTwo) <= 12) {
			newPosition = this.getCurrentPosition() + (moveRollOne + moveRollTwo);
		} else {
			newPosition = this.getCurrentPosition() + (moveRollOne + moveRollTwo) - 12;
			System.out.println("Your proposals have been accepted and your grants have been approved! +100 resources!");
			this.setPlayerResources(this.getPlayerResources() + 100);
		}

		// remove from old square...
		//System.out.println("NEW POSITION: " + newPosition);
		System.out.print("You have moved from " + game.getBoard().get(this.getCurrentPosition()).getName() + "...\n");
		// System.out.println(game.getBoard().get(this.getCurrentPosition() +
		// 1).getSojourners());
		game.getBoard().get(this.getCurrentPosition()).getSojourners()
				.remove(game.getPlayerMap().get(this.getPlayerName()).getPlayerName());
		this.setCurrentPosition(newPosition);
		// ...adding to new square

		game.getBoard().get(this.getCurrentPosition()).getSojourners()
				.add(game.getPlayerMap().get(this.getPlayerName()).getPlayerName());
		System.out.println("...to " + game.getBoard().get(this.getCurrentPosition()).getName());
		System.out.println("\n------------------------------------------------------------");
		System.out.println(game.getBoard().get(this.getCurrentPosition()).display());
		System.out.println("------------------------------------------------------------\n");
		// System.out.println(game.getBoard().get(this.getCurrentPosition() +
		// 1).getSojourners());

		purchase(game, scan);
		collect(game, scan);
	}

	/**
	 * If the Player lands on an un-owned DevSquare they will be asked if they'd
	 * like to purchase it, if they have enough resources. If they choose "Y" they
	 * cost will be deducted from their resource pool and their name will be set as
	 * the owner of the square.
	 * 
	 * @param game
	 * @param scan
	 */
	public void purchase(Game game, Scanner scan) {
		String purchase;
		scan.reset();
		if (game.getBoard().get(this.getCurrentPosition()) instanceof DevSquare) {
			if (((DevSquare) game.getBoard().get(this.getCurrentPosition())).getOwner() == (null)) {
				System.out.println(
						game.getBoard().get(this.getCurrentPosition()).getName() + " is not currently owned...");
				System.out.println("Current R&D Capacity: " + "[" + this.getPlayerResources() + "]");
				System.out.println("Cost: " + ((DevSquare) game.getBoard().get(this.getCurrentPosition())).getCost());
				System.out.println("Would you like to Take the contract? [Y/N]");
				purchase = scan.next();
				if (purchase.equalsIgnoreCase("Y")) {
					System.out.println("Acquiring contract...");
					if (this.getPlayerResources() >= ((DevSquare) game.getBoard().get(this.getCurrentPosition()))
							.getCost()) {
						// subtract cost from resource pool
						this.setPlayerResources(this.getPlayerResources()
								- ((DevSquare) game.getBoard().get(this.getCurrentPosition())).getCost());
						// set square's owner to purchasing player
						((DevSquare) game.getBoard().get(this.getCurrentPosition())).setOwner(this.getPlayerName());
						// add square to player's owned squares list
						this.getOwnedSquares()
								.add(((DevSquare) game.getBoard().get(this.getCurrentPosition())).getSquareID());
						System.out.println("New R&D Capacity: " + this.getPlayerResources());
					} else {
						System.out.println("Unfortunately you do not have the capcity to take on that contract.");
					}
				} else if (purchase.equalsIgnoreCase("N")) {

				} else {
					System.out.println("Please enter either a 'Y' for yes or a 'N' for no.");
				}
			}
		}
	}

	/**
	 * If the player lands on a square owned by another player the owning player
	 * will be asked if they want to collect. If they select "Y" the visitorFee will
	 * be deducted from the landing player and added to the owning player's resource
	 * pool.
	 * 
	 * @param game
	 * @param scan
	 */
	public void collect(Game game, Scanner scan) {
		String collect;
		scan.reset();
		if (game.getBoard().get(this.getCurrentPosition()) instanceof DevSquare) {
			if (((DevSquare) game.getBoard().get(this.getCurrentPosition())).getOwner() != (null)
					&& !((DevSquare) game.getBoard().get(this.getCurrentPosition())).getOwner()
							.equals(this.getPlayerName())) {
				System.out.println(
						((DevSquare) game.getBoard().get(this.getCurrentPosition())).getName() + " is managed by "
								+ ((DevSquare) game.getBoard().get(this.getCurrentPosition())).getOwner() + ".");
				System.out.println("The fee for viewing the contract is "
						+ ((DevSquare) game.getBoard().get(this.getCurrentPosition())).getVisitorFee() + ".");
				System.out.println("Current R&D Capacity: " + "[" + this.getPlayerResources() + "]");
				System.out.println("Does " + ((DevSquare) game.getBoard().get(this.getCurrentPosition())).getOwner()
						+ " want to collect this fee? [Y/N]");
				collect = scan.next();
				if (collect.equalsIgnoreCase("Y")) {
					// subtract fee from the visiting player's resource pool.
					this.setPlayerResources(this.getPlayerResources()
							- ((DevSquare) game.getBoard().get(this.getCurrentPosition())).getVisitorFee());
					// Add fee to owning player's resource pool.
					game.getPlayerMap().get(((DevSquare) game.getBoard().get(this.getCurrentPosition())).getOwner())
							.setPlayerResources(game.getPlayerMap()
									.get(((DevSquare) game.getBoard().get(this.getCurrentPosition())).getOwner())
									.getPlayerResources()
									+ ((DevSquare) game.getBoard().get(this.getCurrentPosition())).getVisitorFee());
					// State transfer amount from visiting player to owning player.
					System.out.println(((DevSquare) game.getBoard().get(this.getCurrentPosition())).getVisitorFee()
							+ " R&D Capacity has been transferred from " + this.getPlayerName() + " to "
							+ ((DevSquare) game.getBoard().get(this.getCurrentPosition())).getOwner());
				} else if (collect.equalsIgnoreCase("N")) {
					System.out.println(((DevSquare) game.getBoard().get(this.getCurrentPosition())).getOwner()
							+ " has graciously decided not to collect the viewing fee on this occasion.");
				} else {
					System.out.println("Please enter either a 'Y' for yes or a 'N' for no.");
				}
			}
		}
	}

	// need to implement loop for incorrect input -> retry option for the above 2
	// methods.

	// .sell()
	public void sell(Game game, Scanner scan) {
		String sell;
		String buyer;
		String tryAgain;
		boolean selling, negotiating;
		List<String> listWithoutOwner;
		List<Integer> soldSquares = new ArrayList<>();
		for (Integer i : this.getOwnedSquares()) { // looking to iterate over an array that has reduced in size. Need to
													// fix this!!!
			// making sure that the seller cannot sell to his or her self
			if (game.getBoard().get(i).getSojourners().contains(this.getPlayerName())) {
				listWithoutOwner = game.getBoard().get(i).getSojourners();
				listWithoutOwner.remove(this.getPlayerName());
			} else {
				listWithoutOwner = game.getBoard().get(i).getSojourners();
			}
			if (listWithoutOwner.size() > 0) {
				if (listWithoutOwner.size() == 1) {
					System.out.println(listWithoutOwner.size() + " player is present on "
							+ ((DevSquare) game.getBoard().get(i)).getName() + ".");
				} else {
					System.out.println(listWithoutOwner.size() + " players are present on "
							+ game.getBoard().get(i).getName() + ".");
				}
				selling = true;
				while (selling) {
					System.out.println(
							"Would you like to sell " + ((DevSquare) game.getBoard().get(i)).getName() + "? [Y/N]");
					sell = scan.next();
					if (sell.equalsIgnoreCase("Y")) {
						negotiating = true;
						while (negotiating == true) {
							if (((DevSquare) game.getBoard().get(i)).getSojourners().size() > 1) {
								System.out.println("Which player do you want to sell to? "
										+ ((DevSquare) game.getBoard().get(i)).getSojourners().toString()
										+ " (Case Sensitive!)");
								buyer = scan.next();
								if (((DevSquare) game.getBoard().get(i)).getSojourners().contains(buyer)) {
									// calls the negotiation function.
									negotiation(((DevSquare) game.getBoard().get(i)), game.getPlayerMap().get(buyer),
											scan);
									soldSquares.add(this.getOwnedSquares()
											.indexOf(((DevSquare) game.getBoard().get(i)).getSquareID()));
									negotiating = false;
								} else {
									System.out.println("Oops, doesn't look like that player is on "
											+ ((DevSquare) game.getBoard().get(i)).getName() + ".");
									System.out.println("Would you like to try again? [Y/N]");
									tryAgain = scan.next();
									if (tryAgain.equals("Y")) {
										continue;
									} else if (tryAgain.equals("N")) {
										System.out.println(this.getPlayerName() + " will hold onto "
												+ ((DevSquare) game.getBoard().get(i)).getName() + " for now.");
										negotiating = false;
									} else {
										System.out.println("Please enter 'Y' for yes or 'N' for no.");
									}
								}
							} else {
								buyer = ((DevSquare) game.getBoard().get(i)).getSojourners().get(0);
								// calls the negotiation function.
								negotiation(((DevSquare) game.getBoard().get(i)), game.getPlayerMap().get(buyer), scan);
								soldSquares.add(this.getOwnedSquares()
										.indexOf(((DevSquare) game.getBoard().get(i)).getSquareID()));
								negotiating = false;
							}
						}
						selling = false;
					} else if (sell.equalsIgnoreCase("N")) {
						System.out.println(this.getPlayerName() + " will hold onto "
								+ ((DevSquare) game.getBoard().get(i)).getName() + " for now.");
						selling = false;
					} else {
						System.out.println("Please enter either a 'Y' for yes or a 'N' for no.");
					}
				}
			} else {
				continue;
			}
		}
		// removing squares from the seller's owned List at the end of the sell method
		// to avoid iterating over a concurrently reducing List.
		for (Integer i : soldSquares) {
			this.getOwnedSquares().remove(this.getOwnedSquares().get(i));
		}
	}

	/**
	 * Starts a negotiation loop between the buyer and owner of the DevSquare on
	 * offer
	 * 
	 * @param ds
	 * @param player
	 * @param scan
	 */
	public void negotiation(DevSquare ds, Player player, Scanner scan) {
		int askingPrice, offer;
		String haggle;
		boolean negotiating = true;

		scan.reset();
		// need to consider input mismatch for offer and asking price...
		while (negotiating) {
			try {
				System.out.println("What is " + this.getPlayerName() + "'s asking price?");
				askingPrice = scan.nextInt();
				System.out.println("What is " + player.getPlayerName() + "'s offer?");
				offer = scan.nextInt();
				if (askingPrice <= offer) {
					System.out.println("An agreement has been reached! " + player.getPlayerName()
							+ " will take over the running of " + ds.getName());
					// exchanging resources
					player.setPlayerResources(player.getPlayerResources() - offer);
					this.setPlayerResources(this.getPlayerResources() + offer);
					// setting new owner of the square
					ds.setOwner(player.getPlayerName());
					player.getOwnedSquares().add(ds.getSquareID());
					negotiating = false;
				} else {
					System.out.println("No agreement: Haggle? [Y/N]");
					haggle = scan.next();
					if (haggle.equalsIgnoreCase("Y")) {
						continue;
					} else if (haggle.equalsIgnoreCase("N")) {
						System.out.println("Is does not seem like an agreement will be reached this time.");
						negotiating = false;
					} else {
						System.out.println("Please select 'Y' for yes or 'N' for no.");
						continue;
					}
				}
			} catch (InputMismatchException ime) {
				System.out.println("Please enter an integer value.");
				scan.next();
			}
		}
	}

	/**
	 * finds the number of complete sets owned by the current player and asks them
	 * if they want to develope them in order. If the player accepts they are asked
	 * which square in the set they want to develope. If they have the resources to
	 * develope that square the Devlevel is increased by +1 and the appropriate cost
	 * is subtracted from the player's resource pool. The player is then asked if
	 * they want to develope another in the set, repeating the process until they
	 * select 'N' for no. At this point the player is presented with the same
	 * options for the next fully owned set until no more exist.
	 * 
	 * @param game
	 * @param scan
	 */
	public void develope(Game game, Scanner scan) throws IllegalArgumentException {
		scan.reset();
		int numberInSet;
		int playerOccurrence;
		int option;
		int selectSquare;
		String userInput;
		boolean another;
		boolean willingToDevelope;
		boolean squareSelected;
		List<DevSquare> squaresInSet = new ArrayList<>();

		// goes through each set and lets the player know if they control all squares in
		// that set.
		for (Integer set : game.getSquareSets()) {
			numberInSet = 0;
			playerOccurrence = 0;
			willingToDevelope = true;
			squaresInSet.clear(); // clears list for each set...
			for (Square s : game.getBoard().values()) {
				if (s instanceof DevSquare) {
					if (((DevSquare) s).getSet() == set) {
						numberInSet++;
						squaresInSet.add(((DevSquare) s));
						if (((DevSquare) s).getOwner() != null) {
							if (((DevSquare) s).getOwner().equals(this.getPlayerName())) {
								playerOccurrence++;
							}
						}

					}
				}
			}
			if (numberInSet == playerOccurrence) {
				System.out.println(this.getPlayerName() + " entirely owns contract set " + set + "!");
				while (willingToDevelope) {
					another = true;
					while (another) {
						squareSelected = false;
						System.out.println("Would you like to develope set " + set + "? [Y/N]");
						userInput = scan.next();
						if (userInput.equalsIgnoreCase("Y")) {
							System.out.println("Which technology would you like to develope?");
							option = 0;
							for (DevSquare s : squaresInSet) {
								option++;
								System.out.println(option + ": " + s.getName());								
							}
							while (!squareSelected) {
								System.out.println("Please enter the number of the technology you want to develope: ");
								try {
									selectSquare = scan.nextInt();
									selectSquare = selectSquare - 1; //makes sure index is one less than user input for list indexing.
									if (selectSquare > 0 && selectSquare <= squaresInSet.size()) {
										// engage IF the player has the resources
										DevelopementLevel oldLevel = ((DevSquare) game.getBoard()
												.get(squaresInSet.get(selectSquare).getSquareID())).getDevLevel();
										if (this.playerResources >= squaresInSet.get(selectSquare - 1).getDevCost()) {

											try {
												// increasing the devLevel of the game's Square.
												((DevSquare) game.getBoard().get(squaresInSet.get(selectSquare)
														.getSquareID())).setDevLevel(squaresInSet.get(selectSquare - 1)
																.getDevLevel()
																.valueOf(((DevSquare) game.getBoard().get(
																		squaresInSet.get(selectSquare).getSquareID()))
																				.getDevLevel().getLevel()
																		+ 1));

												// subtract cost from player's resource pool
												this.setPlayerResources(this.getPlayerResources()
														- squaresInSet.get(selectSquare - 1).getDevCost());
											} catch (IllegalArgumentException illegalargumentexception) {
												System.out.println("Cannot develope further.");
											}

											System.out.println("Developement: " + oldLevel + "->"
													+ ((DevSquare) game.getBoard()
															.get(squaresInSet.get(selectSquare).getSquareID()))
																	.getDevLevel());
											System.out.println(((DevSquare) game.getBoard()
													.get(squaresInSet.get(selectSquare).getSquareID()))
															.getDevDescription());
											squareSelected = true;
										} else {
											System.out.println("Unfortunately you don't have the R&D Capacity to develope "
													+ squaresInSet.get(selectSquare - 1).getName() + " at the moment.");
											squareSelected = true;
										}
									} else {
										System.out.println("Please enter the number of one of the technologies in the set.");
									}
								} catch (InputMismatchException inputmismatchexception) {
									System.out.println("Please enter an integer value.");
									scan.next();
								}
								willingToDevelope = false;
							} // closing 'squareSelected' loop

						} else if (userInput.equalsIgnoreCase("N")) {
							System.out.println("No further developement for set " + set);
							willingToDevelope = false;
							another = false;
						} else {
							System.out.println("Please enter either a 'Y' for yes or a 'N' for no.");
						}
						scan.reset();
					} // closing 'another' loop
				} // closing 'willingToDevelope' loop
			}
		}
	}

}