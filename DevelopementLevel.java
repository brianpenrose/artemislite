/**
 * 
 */
package artemislitegame;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Brian Penrose
 * 
 *         The possible developement levels of developeable squares.
 *
 */
public enum DevelopementLevel {

	L0(0), L1(1), L2(2), L3(3), L4(4);

	private int devLevel;
	private static final Map<Integer, DevelopementLevel> levels = new HashMap<>(); // define static map

	static {
		for (DevelopementLevel devLevel : DevelopementLevel.values()) { // populate static map
			levels.put((Integer) devLevel.getLevel(), devLevel);
		}
	}

	/**
	 * Constructor to associate values with enums
	 * 
	 * @param devLevel
	 */
	private DevelopementLevel(int devLevel) {
		this.devLevel = devLevel;
	}

	/**
	 * Get the enum associated with a given integer
	 * 
	 * @param devLevel
	 * @return
	 */
	public DevelopementLevel valueOf(Integer devLevel) {
		return (DevelopementLevel) levels.get(devLevel);
	}

	/**
	 * Get the integer associated with the given enum
	 * 
	 * @return
	 */
	public int getLevel() {
		return this.devLevel;
	}

	/**
	 * Method accepts a DevelopementLevel and checks if it corresponds to any of the
	 * defined DevelopementLevels in the Enum. It returns a boolean based on whether
	 * the comparison is true or false.
	 * 
	 * @param devLevel
	 * @return
	 */
	public boolean isDefinedEnum(DevelopementLevel devLevel) {
		boolean isPresent = false;
		for (DevelopementLevel level : DevelopementLevel.values()) {
			if (devLevel.getLevel() == level.getLevel()) {
				isPresent = true;
			}
		}
		return isPresent;
	}

}