/**
 * 
 */
package artemislitegame;

/**
 * @author Brian Penrose
 *
 */
public interface IDisplay {
	
	public String display();

}
