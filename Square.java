/**
 * 
 */
package artemislitegame;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Brian Penrose
 *
 */
public class Square implements IDisplay{
	
	private String name;
	private String description;
	private int squareID;
	private List<String> sojourners = new ArrayList<String>();
	
	/**
	 * Default constructor
	 */
	public Square() {}
	
	/**
	 * Constructor with full arguments
	 * @param name
	 * @param description
	 * @param squareID
	 * @param sojourners
	 */
	public Square(String name, String description, int squareID, List<String> sojourners) {
		this.name = name;
		this.description = description;
		this.squareID = squareID;
		this.sojourners = sojourners;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		String temp = "";
		String[] split = description.split(" ");
		for(int counter = 0; counter<split.length; counter++) {
			temp += split[counter] + " ";
			if(counter>0 && counter%10==0) {
				temp += "\n             ";
			}
		}
		this.description = temp;
		
	}

	/**
	 * @return the squareID
	 */
	public int getSquareID() {
		return squareID;
	}

	/**
	 * @param squareID the squareID to set
	 */
	public void setSquareID(int squareID) {
		this.squareID = squareID;
	}

	/**
	 * @return the sojourners
	 */
	public List<String> getSojourners() {
		return sojourners;
	}

	/**
	 * @param sojourners the sojourners to set
	 */
	public void setSojourners(List<String> sojourners) {
		this.sojourners = sojourners;
	}
	
	@Override
	public String display() {
		return "Name: " + this.name + "\nDescription: " + this.description + "\nVisitors: " + this.sojourners.toString() + "\n";
	}

}
