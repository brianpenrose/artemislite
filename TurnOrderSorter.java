/**
 * 
 */
package artemislitegame;

import java.util.Comparator;

/**
 * @author Brian Penrose
 *
 */
public class TurnOrderSorter implements Comparator<Player> {

	@Override
	public int compare(Player p1, Player p2) {
		return p2.getPlayerTurnOrderPosition() - p1.getPlayerTurnOrderPosition();
	}

}
