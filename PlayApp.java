/**
 * 
 */
package artemislitegame;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * @author Brian Penrose
 *
 */
public class PlayApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Game game = new Game();
		Scanner scan = new Scanner(System.in);
		File prologue = new File("prologue.txt");
		File epilogueFailure = new File("epilogue_failure.txt");
		File epilogueSuccess = new File("epilogue_success.txt");

		//prints the prologue to screen...
		try {
			PrologueAndEpilogue.printText(PrologueAndEpilogue.readText(prologue));
		} catch(FileNotFoundException fnfe) {
			System.out.println("Cannot find the Prologue File.");
		}

		//creating a little whitespace
		System.out.println("-------------------------------------------------------------------\n");
		game.playerNumber(scan);

		for (int counter = 1; counter <= game.getNumberOfPlayers(); counter++) {
			game.addPlayer(scan);
		}

		game.turnOrderSorter(game.getPlayers());
		game.createBoard();
		game.createPlayerMap(game.getPlayers());

		System.out.print("Players: ");
		for (Player player : game.getPlayers()) {
			System.out.print(player.getPlayerName() + "[" + player.getPlayerTurnOrderPosition() + "] ");
			game.getBoard().get(1).getSojourners().add(player.getPlayerName());
		}

		System.out.println("\n");

		while (game.resourceCheck() && !game.fullyDeveloped()) {
			for (Player p : game.getPlayers()) {
				
				try {
					Thread.sleep(3000);
					System.out
							.println("-----" + game.getPlayerMap().get(p.getPlayerName()).getPlayerName().toUpperCase()
									+ "'S TURN-----\n");
					System.out.println(game.getPlayerMap().get(p.getPlayerName()).display());
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}

				try {

					Thread.sleep(2000);
					game.getPlayerMap().get(p.getPlayerName()).move(game, scan);
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}

				try {
					Thread.sleep(2000);
					game.getPlayerMap().get(p.getPlayerName()).sell(game, scan);
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}

				try {
					Thread.sleep(2000);
					game.getPlayerMap().get(p.getPlayerName()).develope(game, scan);
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
				
				System.out.println();

			}
		}

		if (game.fullyDeveloped()) {
			try {
				PrologueAndEpilogue.printText(PrologueAndEpilogue.readText(epilogueSuccess));
			} catch(FileNotFoundException fnfe) {
				System.out.println("Cannot find the Successful Epilogue File.");
			}
		} else {
			try {
				PrologueAndEpilogue.printText(PrologueAndEpilogue.readText(epilogueFailure));
			} catch(FileNotFoundException fnfe) {
				System.out.println("Cannot find the Unsuccessful Epilogue File.");
			}
		}

	}

}
