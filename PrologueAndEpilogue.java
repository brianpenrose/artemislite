/**
 * 
 */
package artemislitegame;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Brian Penrose
 *
 */
public class PrologueAndEpilogue {

	/**
	 * Reads a text from a file and stores it line by line in a queue.
	 * 
	 * @param file
	 * @return
	 */
	public static Queue<String> readText(File file) throws FileNotFoundException {
		FileReader fr;
		BufferedReader br;
		String line;
		Queue<String> text = new LinkedList<String>();

		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			do {
				line = br.readLine();
				text.add(line);
			} while (line != null);

		} catch (IOException ioe) {
			System.out.println("Problems writing to the prologue/epilogue files.");
		} 

		return text;
	}

	/**
	 * Takes a String queue and prints the lines to screen with a delay to create a
	 * crawling text effect.
	 * 
	 * @param queue
	 */
	public static void printText(Queue<String> queue) {
		try {
			while(!queue.isEmpty()) {
				if(queue.peek()!=null) {
					System.out.println(queue.remove());
					Thread.sleep(500);
				}else {
					return;
				}
			}
		} catch (InterruptedException interruptexception) {
			interruptexception.printStackTrace();
		}
		
		
	}

}
